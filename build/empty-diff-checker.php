<?php

use DI\ContainerBuilder;
use PhpDotNet\DocTools\Domain\Languages;
use PhpDotNet\DocTools\Renderer\RevCheckRenderer;
use PhpDotNet\DocTools\RevisionCheck\RevisionCheckTable;

$ROOT = \dirname(__DIR__) . \DIRECTORY_SEPARATOR;
$DOCUMENTATION_ROOT = $ROOT;
//$DOCUMENTATION_ROOT = \dirname(__DIR__, 2) . \DIRECTORY_SEPARATOR . 'php-docs/';

require_once $ROOT . 'vendor/autoload.php';

$REVISION_CHECK_FOLDER_PATH = $ROOT . 'public' . RevCheckRenderer::BASE_REVISION_CHECK_PATH;

// Generate PSR-11 Dependency Injection container
$builder = new ContainerBuilder();
$builder->addDefinitions($ROOT . 'config/container-config.php');
$container = $builder->build();
$revisionCheckTable = $container->get(RevisionCheckTable::class);

foreach (Languages::TRANSLATIONS as $language) {
    echo 'Working through '. $language . ' translation: ', \date(DateTimeInterface::W3C), \PHP_EOL;
    $TRANSLATION_PATH = $REVISION_CHECK_FOLDER_PATH . $language . \DIRECTORY_SEPARATOR;
    $resultDirs = $revisionCheckTable->getOutdated($language);
    foreach ($resultDirs as $dir => $files) {
        foreach ($files as $fileInfo) {
            $relativePath = $dir . '/' . $fileInfo['file_name'];
            $relativePath = ltrim($relativePath, '/');  // Handles root elements
            $enRevision = $fileInfo['englishRevision'];
            $currentRevision = $fileInfo['revision'];
            if ($currentRevision === 'n/a') {
                continue;
            }
            $gitDiffCheckCommand = 'git -C ' . $DOCUMENTATION_ROOT . 'en diff ' . $enRevision . ' ' . $currentRevision . ' ' . $relativePath;
            /* git diff produced output */
            if (null !== shell_exec($gitDiffCheckCommand)) {
                continue;
            }

            $absolutePath = $DOCUMENTATION_ROOT . $language . '/' . $relativePath;
            /* Replace Current revision with EN Revision in file */
            $fileContent = file_get_contents($absolutePath);
            $fileContent = str_replace($currentRevision, $enRevision, $fileContent);
            file_put_contents($absolutePath, $fileContent);
        }
    }

    /* Output patch diff into a file */
    $gitPatchCommand = 'git -C ' . $DOCUMENTATION_ROOT . $language . ' diff --output=' . $TRANSLATION_PATH . 'update-revision-empty-change.diff';
    shell_exec($gitPatchCommand);
}
