<?php

declare(strict_types=1);

use PhpDotNet\DocTools\Renderer\HtmlRenderer;

require_once \dirname(__DIR__) . '/vendor/autoload.php';

$parseDown = new Parsedown();

\ob_start();
include_once \dirname(__DIR__) . '/template/layout.php';
$layout = \ob_get_contents();
\ob_end_clean();

$htmlRender = new HtmlRenderer($parseDown);

$dirIterator = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator(\dirname(__DIR__) . \DIRECTORY_SEPARATOR . 'pages')
);

foreach ($dirIterator as $file) {
    if ($file->isDir()) {
        continue;
    }

    $directory = \str_replace('pages', 'public', $file->getPath() . \DIRECTORY_SEPARATOR);
    if (!\is_dir($directory)) {
        \mkdir($directory, 0644, true);
    }

    $markDownContent = \file_get_contents($file->getPathname());
    $generatedHtmlFilePath = $directory . \pathinfo($file->getFileName())['filename'] . '.html';

    $htmlContent = $htmlRender->render($markDownContent, $layout);
    \file_put_contents($generatedHtmlFilePath, $htmlContent);
}
