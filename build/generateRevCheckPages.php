<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use PhpDotNet\DocTools\Domain\Languages;
use PhpDotNet\DocTools\Renderer\RevCheckRenderer;
use PhpDotNet\DocTools\Renderer\TwigRenderer;
use PhpDotNet\DocTools\RevisionCheck\Overview;
use PhpDotNet\DocTools\RevisionCheck\RevisionCheckTable;

$ROOT = \dirname(__DIR__) . \DIRECTORY_SEPARATOR;

require_once $ROOT . 'vendor/autoload.php';

// Generate PSR-11 Dependency Injection container
$builder = new ContainerBuilder();
$builder->addDefinitions($ROOT . 'config/container-config.php');
$container = $builder->build();

$REVISION_CHECK_FOLDER_PATH = $ROOT . 'public' . RevCheckRenderer::BASE_REVISION_CHECK_PATH;

// Insure revision check folder exists
if (!\is_dir($REVISION_CHECK_FOLDER_PATH)) {
    \mkdir($REVISION_CHECK_FOLDER_PATH, 0644, true);
}

$overview = new Overview();

$twigRenderer = new TwigRenderer();
$revisionCheckTable = $container->get(RevisionCheckTable::class);

foreach (Languages::TRANSLATIONS as $language) {
    $TRANSLATION_PATH = $REVISION_CHECK_FOLDER_PATH . $language . \DIRECTORY_SEPARATOR;
    // Insure revision check language folder exists.
    if (!\is_dir($TRANSLATION_PATH)) {
        \mkdir($TRANSLATION_PATH, 0644, true);
    }

    $translation = $revisionCheckTable->getTranslationStatus($language);

    // Common Twig context
    $commonContext = ['language' => $language, 'translation' => $translation];

    // Generate index page
    $indexContent = $twigRenderer->render('revision-check/translation/index.twig', ['language' => $language]);
    \file_put_contents($TRANSLATION_PATH . 'index.html', $indexContent);
    // Generate status page
    $statusContent = $twigRenderer->render('revision-check/translation/status.twig', $commonContext);
    \file_put_contents($TRANSLATION_PATH . 'status.html', $statusContent);
    // Generate graph page
    $graphContent = $twigRenderer->render('revision-check/translation/graph.twig', $commonContext);
    \file_put_contents($TRANSLATION_PATH . 'graph.html', $graphContent);

    // Generate no upstream page
    $result = $revisionCheckTable->getNoUpstream($language);
    $renderContent = $twigRenderer->render(
        'revision-check/translation/no-upstream.twig',
        ['language' => $language, 'directories' => $result, 'amount' => $translation->getNoUpstreamAmount()]
    );
    \file_put_contents($TRANSLATION_PATH . 'no-upstream.html', $renderContent);

    // Generate untranslated page
    $result = $revisionCheckTable->getUntranslated($language);
    $renderContent = $twigRenderer->render(
        'revision-check/translation/untranslated.twig',
        ['language' => $language, 'directories' => $result, 'amount' => $translation->getUntranslatedAmount()]
    );
    \file_put_contents($TRANSLATION_PATH . 'untranslated.html', $renderContent);

    // Generate out of date page
    $result = $revisionCheckTable->getOutdated($language);
    $renderContent = $twigRenderer->render(
        'revision-check/translation/outdated.twig',
        [
            'language' => $language,
            'directories' => $result,
            'amount' => $translation->getOutdatedAmount(),
        ]
    );
    \file_put_contents($TRANSLATION_PATH . 'outdated.html', $renderContent);

    // Generate missing revision page
    $result = $revisionCheckTable->getMissingRevision($language);
    $renderContent = $twigRenderer->render(
        'revision-check/translation/missing-revision.twig',
        ['language' => $language, 'directories' => $result, 'amount' => $translation->getMissingRevisionAmount()]
    );
    \file_put_contents($TRANSLATION_PATH . 'missing-revision.html', $renderContent);

    $overview->addTranslation($language, $translation);
}

// Generate global index
$indexContent = $twigRenderer->render(
    'revision-check/index.twig',
    [
        'nav' => RevCheckRenderer::translationNavigation(Languages::TRANSLATIONS),
        'translations' => $overview->getLanguagesAsCVS(),
        'upToDatePercentages' => $overview->getUpToDatePercentageAsCVS(),
    ]
);
\file_put_contents($REVISION_CHECK_FOLDER_PATH . 'index.html', $indexContent);
