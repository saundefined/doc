<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use PhpDotNet\DocTools\Domain\Languages;
use PhpDotNet\DocTools\Helper\DocFileInfoRetriever;
use PhpDotNet\DocTools\Helper\EnCommitHashesParser;
use PhpDotNet\DocTools\RevisionCheck\Populator;

$ROOT = \dirname(__DIR__) . \DIRECTORY_SEPARATOR;

require_once $ROOT . 'vendor/autoload.php';

// Generate PSR-11 Dependency Injection container
$builder = new ContainerBuilder();
$builder->addDefinitions($ROOT . 'config/container-config.php');
$container = $builder->build();

$DOCUMENTATION_ROOT = $ROOT;
//$DOCUMENTATION_ROOT = \dirname(__DIR__, 2) . \DIRECTORY_SEPARATOR . 'php-docs/';

const IGNORED_TOP_DIRECTORY = [
    'chmonly',
    'internals',
    'internals2',
    '.git',
];

const IGNORED_FILES = [
    'contributors.ent',
    'contributors.xml',
    'README',
    'DO_NOT_TRANSLATE',
    'rsusi.txt',
    'missing-ids.xml',
    'license.xml',
    'translation.xml',
    'versions.xml',
];

const IGNORED_PATHS = [
    'appendices' . \DIRECTORY_SEPARATOR . 'extensions.xml',
    'appendices' . \DIRECTORY_SEPARATOR . 'reserved.constants.xml',
];

const VALID_EXTENSIONS = [
    'xml',
    'ent',
];

$builder = new ContainerBuilder();
$builder->addDefinitions($ROOT . 'config/container-config.php');
$container = $builder->build();

$pdo = $container->get(PDO::class);

$timeStart = new DateTimeImmutable();
echo 'Script start: ', \date(DateTimeInterface::W3C), \PHP_EOL;
$populator = $container->get(Populator::class);

echo 'Extract commit hash for English docs via git log: ', \date(DateTimeInterface::W3C), \PHP_EOL;
$gitLog = shell_exec('git -C ' . $DOCUMENTATION_ROOT . 'en ' . EnCommitHashesParser::GIT_COMMAND);
$parsedLog = EnCommitHashesParser::parseGitLogOutput($gitLog);
echo 'Parse raw log info into file => hash correspondance for ', count($parsedLog), ' files', \PHP_EOL;

$updatedAt = new DateTimeImmutable();
foreach ($parsedLog as $fullFilename => $hash) {
    $pathParts = pathinfo($fullFilename);
    // Extract dir
    $directory = $pathParts['dirname'];
    if ($directory === '.') { $directory = ''; }
    $filename = $pathParts['basename'];

    $populator->insertFileRevisionCheck(
        $directory,
        $filename,
        'en',
        $hash,
        'NULL',
        0,
        $updatedAt
    );
}

echo 'Traverse translations: ', \date(DateTimeInterface::W3C), \PHP_EOL;
foreach (Languages::TRANSLATIONS as $language) {
    $dirIterator = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($DOCUMENTATION_ROOT . $language)
    );

    /**
     * @var SplFileInfo
     */
    foreach ($dirIterator as $file) {
        $directory = \ltrim(\str_replace($DOCUMENTATION_ROOT . $language, '', $file->getPath()), \DIRECTORY_SEPARATOR);
        // Generate top dir
        $topDirectory = \explode(\DIRECTORY_SEPARATOR, $directory, 2)[0];

        if (
            $file->isDir()
            || \in_array($topDirectory, IGNORED_TOP_DIRECTORY, true)
            || !\in_array($file->getExtension(), VALID_EXTENSIONS, true)
            || \in_array($file->getFilename(), IGNORED_FILES, true)
            || \in_array($directory . \DIRECTORY_SEPARATOR . $file->getFilename(), IGNORED_PATHS, true)
            || str_starts_with($file->getFilename(), 'entities.') // Ignore entities files
            // || substr($file->getFilename(), -13) != 'PHPEditBackup') // Something from the original code
        ) {
            continue;
        }

        $size = (int) \round(\filesize($file->getPathname()) / 1024);

        try {
            // Use '@' format to pass Unix timestamp
            $updatedAt = new DateTimeImmutable('@' . \filemtime($file->getPathname()));

            // Read the first 500 chars. The comment should be at the beginning of the file
            $fileResource = @\fopen($file->getPathname(), 'rb');
            if ($fileResource === false) {
                throw new Exception("Unable to read {$file}.");
            }
            $extract = \fread($fileResource, 500);
            \fclose($fileResource);
        } catch (Exception $exception) {
            echo $exception->getMessage() . \PHP_EOL;

            continue;
        }

        $info = DocFileInfoRetriever::getTranslatedFileInformation($extract);
        $revision = $info->getEnglishRevision();
        $maintainer = $info->getMaintainer();

        $populator->insertFileRevisionCheck(
            $directory,
            $file->getFilename(),
            $language,
            $revision,
            $maintainer,
            $size,
            $updatedAt
        );
    }
}

$time = $timeStart->diff(new DateTimeImmutable());
echo 'Parsed ' . $populator->getNumberOfFilesPopulated() . ' in ' .
    $time->format('%h hours %i minutes and %s,%F seconds') . \PHP_EOL;
echo 'End' . \PHP_EOL;
