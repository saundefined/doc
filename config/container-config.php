<?php

declare(strict_types=1);

$DB_PATH = \dirname(__DIR__) . \DIRECTORY_SEPARATOR . 'data' . \DIRECTORY_SEPARATOR;
// Insure database folder exists.
if (!\is_dir($DB_PATH)) {
    \mkdir($DB_PATH, 0644, true);
}

return [
    PDO::class => new PDO('sqlite:' . $DB_PATH . 'revcheck-dev.sqlite3', null, null, [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    ]),
];
