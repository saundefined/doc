<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class LanguageRevisionCheck extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('files');
        $table->addColumn('directory', 'string')
            ->addColumn('file_name', 'string')
            ->addColumn('file_language', 'string')
            ->addColumn('revision', 'string')
            ->addColumn('maintainer', 'string')
            ->addColumn('size', 'integer')
            ->addColumn('updated_at', 'datetime')
            ->addIndex('file_language')
            ->addIndex('revision')
            ->addIndex(['directory', 'file_name', 'file_language'], ['unique' => true])
            ->create()
        ;
    }
}
