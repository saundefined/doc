<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

final class RevCheckSeeder extends AbstractSeed
{
    private const TRANSLATION_LANGUAGE = 'fr';
    private const UP_TO_DATE = 1;
    private const OUTDATED = 2;
    private const MISSING = 3;

    private $data = [];

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(): void
    {
        $this->faker = Faker\Factory::create();
        $this->generateUpToDateEntries(500);
        $this->generateOutdated(200);
        $this->generateMissing(15);
        $this->generateNoUpstream(20);
        $this->generateUntranslated(400);

        $this->table('files')->insert($this->data)->save();
    }

    private function generateUpToDateEntries(int $amount): void
    {
        for ($i = 0; $i < $amount; ++$i) {
            $this->generatePairEntry(self::UP_TO_DATE);
        }
    }

    private function generateOutdated(int $amount): void
    {
        for ($i = 0; $i < $amount; ++$i) {
            $this->generatePairEntry(self::OUTDATED);
        }
    }

    private function generateMissing(int $amount): void
    {
        for ($i = 0; $i < $amount; ++$i) {
            $this->generatePairEntry(self::MISSING);
        }
    }

    private function generateUntranslated(int $amount): void
    {
        for ($i = 0; $i < $amount; ++$i) {
            $this->generateEntry(null);
        }
    }

    private function generateNoUpstream(int $amount): void
    {
        for ($i = 0; $i < $amount; ++$i) {
            $this->generateEntry(self::TRANSLATION_LANGUAGE);
        }
    }

    private function generateEntry(?string $language = null): void
    {
        // Upgrade to ??= in PHP 7.4
        $language = $language ?? 'en';
        $this->data[] = [
            'directory' => $this->generateFileName(),
            'file_name' => $this->generateFileName() . 'xml',
            'file_language' => $language,
            'revision' => $this->faker->unique()->sha1,
            'maintainer' => $this->faker->userName,
            'size' => $this->faker->randomNumber(),
            'updated_at' => $this->faker->dateTime()->format('Y-m-d H:i:s'),
        ];
    }

    private function generatePairEntry(int $flag): void
    {
        $directory = $this->generateFileName();
        $file = $this->generateFileName() . '.xml';

        $en = $translation = [
            'directory' => $directory,
            'file_name' => $file,
            'maintainer' => $this->faker->userName,
            'size' => $this->faker->randomNumber(),
            'updated_at' => $this->faker->dateTime()->format('Y-m-d H:i:s'),
        ];

        $en['file_language'] = 'en';
        $translation['file_language'] = self::TRANSLATION_LANGUAGE;

        if ($flag === self::UP_TO_DATE) {
            $revision = $this->faker->unique()->sha1;
            $en['revision'] = $revision;
            $translation['revision'] = $revision;
        }
        if ($flag === self::OUTDATED) {
            $en['revision'] = $this->faker->unique()->sha1;
            $translation['revision'] = $this->faker->unique()->sha1;
        }
        if ($flag === self::MISSING) {
            $en['revision'] = $this->faker->unique()->sha1;
            $translation['revision'] = null;
        }

        if (\mt_rand(0, 100) < 50) {
            $otherTranslation = $translation;
            $otherTranslation['file_language'] = 'zh';
            $this->data[] = $otherTranslation;
        }

        $this->data[] = $en;
        $this->data[] = $translation;
    }

    private function generateFileName(): string
    {
        $fileName = '';
        for ($i = 0; $i <= $this->faker->numberBetween(4, 65); ++$i) {
            $fileName .= $this->faker->randomLetter;
        }

        return $fileName;
    }
}
