## What is this site?

There are different documentation teams operating somewhat independently on
PHP.net sub-projects. Only a few people are involved in more than one
documentation project currently, and that limits the know-how sharing,
which would otherwise be possible and desirable.
All of the documentation teams use an early fork of the original PHP manual
build system. These systems evolved independently, and took over some
improvements from the others on an occasional basis (the PEAR documentation
team uses a variant of the revcheck developed for phpdoc for example).
This site is aimed at getting documentation efforts closer, and provide more
tools and information for manual authors and translators.

## Join Us!

The developers of PHP extensions all
have access to the PHP documentation modules, although the different projects
have different policies of accepting contributions. You are welcome to join
our efforts! Authoring documentation for an extension is the second best thing
to implementing the extension itself if you are interested in the
capabilities, features and usage possibilities even in extreme conditions. We
also have translated versions of the different manuals, and most of the
translation teams are always in need of more helping hands. In case you are
interested, you should contact the relevant mailing lists. The projects and
translations have separate mailing lists.
