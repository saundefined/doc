# PHP Manual Contribution Guide
## Introduction
PHP is well known for having excellent documentation. Documentation that is created by volunteers who
collectively make changes every day. This guide is designed for people who work on the official PHP documentation.

## Glossary
This guide uses some terminology that you have to know. Don't worry, it's easy:
- **author** - person who contributes to the original English manual
- **translator** - person who translates the English manual into another language
- **{LANG}** - replace it with your two-letter country code, (e.g. when referring
  to a mailinglist, `doc-{LANG}@lists.php.net`). Note: Brazilian Portuguese differs
  from the rest and it's called *pt_BR* for the SVN module and *pt-br* for the
  mailing list suffix.

## Table of Contents
- [Joining the team](joining.html)
- [Documentation structure](structure.html)
- [Editing the PHP Manual](editing.html)
- [Translating documentation](translating.html)
- [Style guidelines](style.html)

## Appendices
- [FAQ](faq.html)
- [PHP Manual builds](builds.html)
- [Why whitespace is important in phpdoc](whitespace.html)
- [User Note Editing Guidelines](user-notes.html)
- [Setting up Documentation environment](local-setup.html)

## Feedback
Feedback is most certainly welcome on this document. Without your submissions and input, this document wouldn't exist.
Please send your additions, comments and criticisms to the following email address: phpdoc@lists.php.net.