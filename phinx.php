<?php

declare(strict_types=1);

return
[
    'paths' => [
        'migrations' => __DIR__ . '/db/migrations',
        'seeds' => __DIR__ . '/db/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'production' => [
            'adapter' => 'sqlite',
            'name' => './data/revcheck',
        ],
        'development' => [
            'adapter' => 'sqlite',
            'name' => './data/revcheck-dev',
        ],
        'testing' => [
            'adapter' => 'sqlite',
            'name' => 'test',
            'memory' => true,
        ],
    ],
    'version_order' => 'creation',
];
