<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\Domain;

final class Languages
{
    public const ALL = ([self::ENGLISH] + self::TRANSLATIONS);
    public const TRANSLATIONS = [
        self::BRAZILIAN_PORTUGUESE,
        self::CHINESE_SIMPLIFIED,
        self::FRENCH,
        self::GERMAN,
        self::ITALIAN,
        self::JAPANESE,
        self::POLISH,
        self::ROMANIAN,
        self::RUSSIAN,
        self::SPANISH,
        self::TURKISH,
        self::UKRAINIAN,
    ];
    private const ENGLISH = 'en';
    private const ARABIC = 'ar';
    private const BRAZILIAN_PORTUGUESE = 'pt_br';
    private const BULGARIAN = 'bg';
    private const CHINESE_SIMPLIFIED = 'zh';
    private const CHINESE_HONG_KONG_CANTONESE = 'hk';
    private const CHINESE_TRADITIONAL = 'tw';
    private const CZECH = 'cs';
    private const DANISH = 'da';
    private const DUTCH = 'nl';
    private const FINNISH = 'fi';
    private const FRENCH = 'fr';
    private const GERMAN = 'de';
    private const GREEK = 'el';
    private const HEBREW = 'he';
    private const HUNGARIAN = 'hu';
    private const ITALIAN = 'it';
    private const JAPANESE = 'ja';
    private const KOREAN = 'kr';
    private const NORWEGIAN = 'no';
    private const PERSIAN = 'fa';
    private const POLISH = 'pl';
    private const PORTUGUESE = 'pt';
    private const ROMANIAN = 'ro';
    private const RUSSIAN = 'ru';
    private const SERBIAN = 'sr';
    private const SLOVAK = 'sk';
    private const SLOVENIAN = 'sl';
    private const SPANISH = 'es';
    private const SWEDISH = 'sv';
    private const TURKISH = 'tr';
    private const UKRAINIAN = 'uk';
}
