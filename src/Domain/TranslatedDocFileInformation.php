<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\Domain;

final class TranslatedDocFileInformation
{
    public function __construct(private string $englishRevision, private string $maintainer, private string $status)
    {
    }

    public function getEnglishRevision(): string
    {
        return $this->englishRevision;
    }

    public function getMaintainer(): string
    {
        return $this->maintainer;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
