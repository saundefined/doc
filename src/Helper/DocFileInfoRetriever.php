<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\Helper;

use PhpDotNet\DocTools\Domain\TranslatedDocFileInformation;

final class DocFileInfoRetriever
{
    private const REVISION_TAG_REGEX = '/<!-- .Revision: (\d+) . -->/';
    private const COMMON_REGEX = '\s*Maintainer:\s*(\S*)\s*Status:\s*(.+)\s*-->#U';
    private const FILE_INFO_GIT_REGEX = '#<!--\s*EN-Revision:\s*([0-9a-f]{40})' . self::COMMON_REGEX;
    private const FILE_INFO_NA_REGEX = '#<!--\s*EN-Revision:\s*(n/a)' . self::COMMON_REGEX;

    public static function getRevision(string $extract): string
    {
        \preg_match(self::REVISION_TAG_REGEX, $extract, $match);
        if (!empty($match)) {
            return $match[1];
        }

        return '0';
    }

    public static function getTranslatedFileInformation(string $extract): TranslatedDocFileInformation
    {
        // No matches before the preg
        $matches = [];
        // Check for the translations "revision tag"
        if (\preg_match(self::FILE_INFO_GIT_REGEX, $extract, $matches)) {
            return new TranslatedDocFileInformation(\trim($matches[1]), \trim($matches[2]), \trim($matches[3]));
        }

        // The tag with revision number is not found so search for n/a revision comment
        if (\preg_match(self::FILE_INFO_NA_REGEX, $extract, $matches)) {
            return new TranslatedDocFileInformation(\trim($matches[1]), \trim($matches[2]), \trim($matches[3]));
        }
        // Nothing, return with NULL values
        return new TranslatedDocFileInformation('NULL', 'NULL', 'NULL');
    }
}
