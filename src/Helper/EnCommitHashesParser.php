<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\Helper;

final class EnCommitHashesParser
{
    public const GIT_COMMAND = 'log --raw --format="%H"';
    public const GIT_RAW_CHANGE_TYPE_OFFSET = 37;
    private const IGNORE_GIT_COMMIT = [
        // XML Formatting commits
        '8d666e819852f6b0561b40fcf8689b747568865c',
        // vrana's mass commit
        '69bd53265b6c8cf0ba4135142488de6dcb78e4eb',
        // the revert commit
        '4754397753fd79f1c846868b66a2448babab1c54',
    ];

    /**
     * The input has the following strucutre:
     * 40 char Sha1 Hash
     * blank line
     * :(39 bytes)filename (1 or more times)
     * [back to start]
     */
    public static function parseGitLogOutput(string $rawInput): array
    {
        $stream = fopen('php://memory', 'wb+');
        fwrite($stream, $rawInput);
        rewind($stream);
        $hash = '';
        $output = [];
        $unsettedFiles = [];
        while (($line = fgets($stream)) !== false) {
            if (str_starts_with($line, ':')) {
                /* Renames need special handling */
                /**
                 * Format is (as of byte GIT_RAW_CHANGE_TYPE_OFFSET):
                 * R(\d+)\t(old_file_name)\t(new_file_name)
                 */
                if ($line[self::GIT_RAW_CHANGE_TYPE_OFFSET] === 'R') {
                    // Find pos of tabs:
                    // +1 to get offset of first byte of file name
                    $tab1 = ((int) strpos($line, "\t", self::GIT_RAW_CHANGE_TYPE_OFFSET) + 1);
                    $tab2  = (int) strpos($line, "\t", $tab1);
                    $fileNameLength = ($tab2 - $tab1);
                    $fileName = substr($line, $tab1, $fileNameLength);
                    $unsettedFiles[] = $fileName;
                    continue;
                }

                /* parse file names */
                $fileName = rtrim(substr($line, 39));
                if ($line[self::GIT_RAW_CHANGE_TYPE_OFFSET] === 'D') {
                    $unsettedFiles[] = $fileName;
                }
                if (!\array_key_exists($fileName, $output)) {
                    if (
                        self::isIgnoredPath($fileName)
                        || \in_array($fileName, $unsettedFiles, true)
                        || \in_array($hash, self::IGNORE_GIT_COMMIT)
                    ) {
                        continue;
                    }
                    $output[$fileName] = $hash;
                }
                continue;
            }
            if (\strlen($line) >= 40) {
                $hash = trim($line);
            }
        }
        return $output;
    }

    private static function isIgnoredPath(string $filename): bool
    {
        return (
            // File extensions
            (!str_ends_with($filename, '.xml') && !str_ends_with($filename, '.ent'))
            // Files
            || str_ends_with($filename, 'contributors.ent')
            || str_ends_with($filename, 'contributors.xml')
            || str_ends_with($filename, 'missing-ids.xml')
            || str_ends_with($filename, 'license.xml')
            || str_ends_with($filename, 'translation.xml')
            || str_ends_with($filename, 'versions.xml')
            // Folders
            || str_starts_with($filename, 'internals/')
            || str_starts_with($filename, 'internals2/')
            || str_starts_with($filename, 'chmonly/')
            // Paths
            || $filename === 'appendices/extensions.xml'
            || $filename === 'appendices/reserved.constants.xml'
        );
    }
}
