<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\Renderer;

use Parsedown;

final class HtmlRenderer
{
    public const LAYOUT_CONTENT_TAG = '<!-- CONTENT TO INSERT HERE -->';

    private Parsedown $parseDown;

    public function __construct(Parsedown $parseDown)
    {
        $this->parseDown = $parseDown;
    }

    public function render(string $markdownContent, string $layout = self::LAYOUT_CONTENT_TAG): string
    {
        /** @var string $htmlContent */
        $htmlContent = $this->parseDown->text($markdownContent);

        return \str_replace(self::LAYOUT_CONTENT_TAG, $htmlContent, $layout);
    }
}
