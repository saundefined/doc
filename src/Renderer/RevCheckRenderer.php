<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\Renderer;

final class RevCheckRenderer
{
    public const BASE_REVISION_CHECK_PATH = '/tools/revcheck/';

    /**
     * @param iterable<string> $languages
     */
    public static function translationNavigation(iterable $languages): string
    {
        $list = '';

        foreach ($languages as $language) {
            $list .= '<li><a href="' . self::BASE_REVISION_CHECK_PATH . $language . '">' . $language . '</a></li>';
        }

        return $list;
    }
}
