<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\Renderer;

use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

final class TwigRenderer
{
    /**
     * @var Environment
     */
    private $twig;

    public function __construct()
    {
        $loader = new FilesystemLoader('template');
        $twig = new Environment($loader, ['debug' => true]);
        // Debug extension!
        $twig->addExtension(new DebugExtension());
        $this->twig = $twig;
    }

    public function render(string $template, array $variables): string
    {
        $context = \array_merge([
            'revisionCheckPath' => RevCheckRenderer::BASE_REVISION_CHECK_PATH,
        ], $variables);

        return $this->twig->render($template, $context);
    }
}
