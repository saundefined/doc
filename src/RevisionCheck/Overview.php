<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck;

use PhpDotNet\DocTools\RevisionCheck\Status\Translation;

final class Overview
{
    /**
     * @var array<string, float>
     */
    private $translationStatus = [];

    public function addTranslation(string $language, Translation $translation): void
    {
        $this->translationStatus[$language] = $translation->getUpToDateAmountPercentage();
    }

    public function getLanguagesAsCVS(): string
    {
        return "'" . \implode('\', \'', \array_keys($this->translationStatus)) . "'";
    }

    public function getUpToDatePercentageAsCVS(): string
    {
        return \implode(', ', $this->translationStatus);
    }
}
