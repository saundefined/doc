<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck;

use PDO;
use PDOStatement;

final class Populator
{
    private const DATE_FORMAT = 'Y-m-d H:i:s';

    private const INSERT_QUERY = 'INSERT INTO files (directory, file_name, file_language, revision, maintainer, size,
updated_at) VALUES (:directory, :name, :language, :revision, :maintainer, :size, :updated_at)';

    /**
     * @var PDOStatement
     */
    private $statement;

    /**
     * @var int
     */
    private $nbFiles = 0;

    public function __construct(PDO $PDO)
    {
        $this->statement = $PDO->prepare(self::INSERT_QUERY);
    }

    public function insertFileRevisionCheck(
        string $directory,
        string $fileName,
        string $language,
        string $revision,
        string $maintainer,
        int $size,
        \DateTimeInterface $updatedAt
    ): void {
        $this->statement->bindValue('directory', $directory, PDO::PARAM_STR);
        $this->statement->bindValue('name', $fileName, PDO::PARAM_STR);
        $this->statement->bindValue('language', $language, PDO::PARAM_STR);
        $this->statement->bindValue('size', $size, PDO::PARAM_STR);
        $this->statement->bindValue('updated_at', $updatedAt->format(self::DATE_FORMAT), PDO::PARAM_STR);

        if ($revision === 'NULL') {
            $this->statement->bindValue('revision', null, PDO::PARAM_NULL);
        } else {
            $this->statement->bindValue('revision', $revision, PDO::PARAM_STR);
        }
        if ($maintainer === 'NULL') {
            $this->statement->bindValue('maintainer', null, PDO::PARAM_NULL);
        } else {
            $this->statement->bindValue('maintainer', $maintainer, PDO::PARAM_STR);
        }

        $this->statement->execute();
        ++$this->nbFiles;
    }

    public function getNumberOfFilesPopulated(): int
    {
        return $this->nbFiles;
    }
}
