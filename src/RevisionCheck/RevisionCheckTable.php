<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck;

use PDO;
use PDOStatement;
use PhpDotNet\DocTools\RevisionCheck\Status\FileStatus;
use PhpDotNet\DocTools\RevisionCheck\Status\MissingRevision;
use PhpDotNet\DocTools\RevisionCheck\Status\NoUpstream;
use PhpDotNet\DocTools\RevisionCheck\Status\Outdated;
use PhpDotNet\DocTools\RevisionCheck\Status\Translation;
use PhpDotNet\DocTools\RevisionCheck\Status\Untranslated;
use PhpDotNet\DocTools\RevisionCheck\Status\UpToDate;

final class RevisionCheckTable
{
    private const COMMON_METADATA_SQL = 'SELECT COUNT(1) AS nbFiles, SUM(size) AS sumSize FROM files WHERE';

    private const UNTRANSLATED_SET = "SELECT directory, file_name FROM files WHERE file_language = 'en'
EXCEPT
SELECT directory, file_name FROM files WHERE file_language = :language";

    private const NO_ENGLISH_UPSTREAM_SET = "SELECT directory, file_name FROM files WHERE file_language = :language
EXCEPT
SELECT directory, file_name FROM files WHERE file_language = 'en'";

    private const OUTDATED_SET = 'SELECT directory, file_name, revision FROM files
WHERE file_language = :language AND (directory, file_name) NOT IN (' . self::NO_ENGLISH_UPSTREAM_SET . ")
EXCEPT
SELECT directory, file_name, revision FROM files WHERE file_language = 'en'";

    private const UP_TO_DATE_SET = "SELECT directory, file_name, revision FROM files WHERE file_language = :language
INTERSECT
SELECT directory, file_name, revision FROM files WHERE file_language = 'en'";

    /**
     * @var PDO
     */
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getTranslationStatus(string $language): Translation
    {
        /**
         * @var UpToDate
         */
        $upToDate = $this->getMetaData(UpToDate::class, $language);
        /**
         * @var Outdated
         */
        $outDated = $this->getMetaData(Outdated::class, $language);
        /**
         * @var MissingRevision
         */
        $missing = $this->getMetaData(MissingRevision::class, $language);
        /**
         * @var Untranslated
         */
        $untranslated = $this->getMetaData(Untranslated::class, $language);
        /**
         * @var NoUpstream
         */
        $noUpstream = $this->getMetaData(NoUpstream::class, $language);

        return new Translation($upToDate, $outDated, $missing, $untranslated, $noUpstream);
    }

    public function getUntranslated(string $language): array
    {
        $query = $this->buildQuery(self::UNTRANSLATED_SET);

        return $this->executeQuery($query, $language)->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);
    }

    public function getNoUpstream(string $language): array
    {
        $query = $this->buildQuery(self::NO_ENGLISH_UPSTREAM_SET);

        return $this->executeQuery($query, $language)->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);
    }

    public function getMissingRevision(string $language): array
    {
        $query = 'SELECT lang.directory, lang.file_name, lang.size, en.size AS englishSize FROM files AS lang
LEFT OUTER JOIN files AS en ON en.directory = lang.directory AND en.file_name = lang.file_name
WHERE lang.file_language = :language AND lang.revision IS NULL 
GROUP BY lang.directory, lang.file_name ORDER BY lang.directory, lang.file_name';

        return $this->executeQuery($query, $language)->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);
    }

    public function getOutdated(string $language): array
    {
        $query = "SELECT lang.directory, lang.file_name, lang.maintainer, lang.revision, en.revision AS englishRevision
FROM files AS lang
LEFT OUTER JOIN files AS en ON lang.directory = en.directory AND lang.file_name = en.file_name
WHERE lang.file_language = :language AND en.file_language = 'en'AND (lang.directory, lang.file_name, lang.revision) IN (
    SELECT directory, file_name, revision
    FROM files
    WHERE file_language = :language AND (directory, file_name) NOT IN (
        " . self::NO_ENGLISH_UPSTREAM_SET . "
    )
    EXCEPT
    SELECT directory, file_name, revision FROM files WHERE file_language = 'en'
)
GROUP BY lang.directory, lang.file_name ORDER BY lang.directory, lang.file_name";

        return $this->executeQuery($query, $language)->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);
    }

    /**
     * @param MissingRevision::class|NoUpstream::class|Outdated::class|Untranslated::class|UpToDate::class
     * $fileStatusClass
     */
    private function getMetaData(string $fileStatusClass, string $language): FileStatus
    {
        $whereLangClause = ' file_language = :language AND ';
        switch ($fileStatusClass) {
            case MissingRevision::class:
                $query = self::COMMON_METADATA_SQL . $whereLangClause . 'revision IS NULL';

                break;
            case NoUpstream::class:
                $query = self::COMMON_METADATA_SQL . $whereLangClause .
                    $this->getWhereInClause(self::NO_ENGLISH_UPSTREAM_SET);

                break;
            case Outdated::class:
                $query = self::COMMON_METADATA_SQL . $whereLangClause .
                    $this->getWhereInClause(self::OUTDATED_SET);

                break;
            case Untranslated::class:
                $query = self::COMMON_METADATA_SQL . $this->getWhereInClause(self::UNTRANSLATED_SET);

                break;
            case UpToDate::class:
                $query = self::COMMON_METADATA_SQL . $whereLangClause .
                    $this->getWhereInClause(self::UP_TO_DATE_SET);

                break;
        }

        $statement = $this->executeQuery($query, $language);
        /**
         * @var array
         */
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        return new $fileStatusClass((int) $result['nbFiles'], (int) $result['sumSize']);
    }

    private function executeQuery(string $query, string $language): PDOStatement
    {
        $statement = $this->pdo->prepare($query);
        $statement->bindValue('language', $language, PDO::PARAM_STR);
        $statement->execute();

        return $statement;
    }

    private function buildQuery(string $innerQuery): string
    {
        return 'SELECT directory, file_name, size, maintainer, revision FROM files WHERE '
            . $this->getWhereInClause($innerQuery) . ' GROUP BY directory, file_name ORDER BY directory, file_name';
    }

    private function getWhereInClause(string $innerQuery): string
    {
        if ($innerQuery === self::UP_TO_DATE_SET || $innerQuery === self::OUTDATED_SET) {
            return "(directory, file_name, revision) IN ({$innerQuery})";
        }

        return "(directory, file_name) IN ({$innerQuery})";
    }
}
