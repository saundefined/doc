<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck\Status;

interface FileStatus
{
    /**
     * Amount of files with the target status.
     */
    public function amount(): int;

    /**
     * Size in KiloBytes of the files with this status.
     */
    public function size(): int;
}
