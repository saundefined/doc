<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck\Status;

abstract class Files implements FileStatus
{
    /**
     * @var int
     */
    private $amount;
    /**
     * @var int
     */
    private $size;

    public function __construct(int $amount, int $size)
    {
        if ($amount < 0) {
            throw new \InvalidArgumentException('Amount of files must be greater or equal than zero');
        }
        if ($size < 0) {
            throw new \InvalidArgumentException('Size of files must be greater or equal than zero');
        }
        $this->amount = $amount;
        $this->size = $size;
    }

    /**
     * Amount of files with the target status.
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * Size in KiloBytes of the files with this status.
     */
    public function size(): int
    {
        return $this->size;
    }
}
