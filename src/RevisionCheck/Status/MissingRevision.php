<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck\Status;

final class MissingRevision extends Files implements FileStatus
{
}
