<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck\Status;

final class Total extends Files implements FileStatus
{
    public function __construct(
        UpToDate $upToDate,
        Outdated $outDated,
        MissingRevision $missing,
        Untranslated $untranslated
    ) {
        $amount = ($upToDate->amount() + $outDated->amount() + $missing->amount() + $untranslated->amount());
        $size = ($upToDate->size() + $outDated->size() + $missing->size() + $untranslated->size());
        parent::__construct($amount, $size);
    }
}
