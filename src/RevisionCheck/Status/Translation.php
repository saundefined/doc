<?php

declare(strict_types=1);

namespace PhpDotNet\DocTools\RevisionCheck\Status;

final class Translation
{
    /**
     * @var Total
     */
    private $total;
    /**
     * @var Outdated
     */
    private $outdated;
    /**
     * @var MissingRevision
     */
    private $missingRevision;
    /**
     * @var Untranslated
     */
    private $untranslated;
    /**
     * @var UpToDate
     */
    private $upToDate;
    /**
     * @var NoUpstream
     */
    private $noUpstream;

    public function __construct(
        UpToDate $upToDate,
        Outdated $outdated,
        MissingRevision $missingRevision,
        Untranslated $untranslated,
        NoUpstream $noUpstream
    ) {
        $this->upToDate = $upToDate;
        $this->outdated = $outdated;
        $this->missingRevision = $missingRevision;
        $this->untranslated = $untranslated;
        $this->total = new Total($upToDate, $outdated, $missingRevision, $untranslated);
        $this->noUpstream = $noUpstream;
    }

    public function getOutdatedAmount(): int
    {
        return $this->outdated->amount();
    }

    public function getOutdatedAmountPercentage(): float
    {
        return $this->getFilesPercentage($this->outdated);
    }

    public function getOutdatedSize(): int
    {
        return $this->outdated->size();
    }

    public function getOutdatedSizePercentage(): float
    {
        return $this->getSizePercentage($this->outdated);
    }

    public function getMissingRevisionAmount(): int
    {
        return $this->missingRevision->amount();
    }

    public function getMissingRevisionAmountPercentage(): float
    {
        return $this->getFilesPercentage($this->missingRevision);
    }

    public function getMissingRevisionSize(): int
    {
        return $this->missingRevision->size();
    }

    public function getMissingRevisionSizePercentage(): float
    {
        return $this->getSizePercentage($this->missingRevision);
    }

    public function getUntranslatedAmount(): int
    {
        return $this->untranslated->amount();
    }

    public function getUntranslatedAmountPercentage(): float
    {
        return $this->getFilesPercentage($this->untranslated);
    }

    public function getUntranslatedSize(): int
    {
        return $this->untranslated->size();
    }

    public function getUntranslatedSizePercentage(): float
    {
        return $this->getSizePercentage($this->untranslated);
    }

    public function getTotalAmount(): int
    {
        return $this->total->amount();
    }

    public function getTotalAmountPercentage(): float
    {
        return $this->getFilesPercentage($this->total);
    }

    public function getTotalSize(): int
    {
        return $this->total->size();
    }

    public function getTotalSizePercentage(): float
    {
        return $this->getSizePercentage($this->total);
    }

    public function getUpToDateAmount(): int
    {
        return $this->upToDate->amount();
    }

    public function getUpToDateAmountPercentage(): float
    {
        return $this->getFilesPercentage($this->upToDate);
    }

    public function getUpToDateSize(): int
    {
        return $this->upToDate->size();
    }

    public function getUpToDateSizePercentage(): float
    {
        return $this->getSizePercentage($this->upToDate);
    }

    public function getNoUpstreamAmount(): int
    {
        return $this->noUpstream->amount();
    }

    public function getNoUpstreamSize(): int
    {
        return $this->noUpstream->size();
    }

    private function getFilesPercentage(FileStatus $fileStatus): float
    {
        if ($this->total->amount() === 0) {
            return 0;
        }

        return \round((($fileStatus->amount() / $this->total->amount()) * 100), 2);
    }

    private function getSizePercentage(FileStatus $fileStatus): float
    {
        if ($this->total->amount() === 0) {
            return 0;
        }

        return \round((($fileStatus->size() / $this->total->size()) * 100), 2);
    }
}
