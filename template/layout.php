<?php

declare(strict_types=1);

use PhpDotNet\DocTools\Renderer\HtmlRenderer;
use PhpDotNet\DocTools\Renderer\RevCheckRenderer;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PHP: Documentation Tools</title>

    <link media='all' rel='stylesheet' href='//shared.php.net/styles/defaults.css?filemtime=1564078161'/>
    <link media='all' rel='stylesheet' href='//shared.php.net/styles/doc.css?filemtime=1564078161'/>

    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,400italic,600italic|Source+Code+Pro&amp;
    subset=latin,latin-ext" rel="stylesheet">
    <link rel="shortcut icon" href="//php.net/favicon.ico">
</head>
<body>
<header class='clearfix'>
    <div id="mainmenu-toggle-overlay"></div>
    <input type="checkbox" id="mainmenu-toggle">
    <nav class="fullscreen">
        <div class="mainscreen">
            <a href="/" class="home">
                <img src="//php.net/images/logo.php?doc" width="48" height="24" alt="php">
                <span class="subdomain">doc</span>
            </a>
            <ul>
                <li><a href="<?php echo RevCheckRenderer::BASE_REVISION_CHECK_PATH; ?>">Documentation Tools</a></li>
                <li><a href="/tutorial/">Tutorial for Contributors</a></li>
                <li><a href="/phd/">PhD Homepage</a></li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrap">
    <?php echo HtmlRenderer::LAYOUT_CONTENT_TAG; ?>
</div>
<footer>
    <nav class="fullscreen">
        <ul>
            <li><a href="//php.net/copyright">Copyright © 2001-2019 The PHP Group</a></li>
            <li><a href="//php.net/sites">Other PHP.net sites</a></li>
            <li><a href="//php.net/privacy">Privacy policy</a></li>
        </ul>
    </nav>
</footer>
</body>
