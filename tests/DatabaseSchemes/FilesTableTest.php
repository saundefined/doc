<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\DatabaseSchemes;

use PDO;
use Tests\PhpDotNet\DocTools\TestCases\DatabaseTestCase;

/**
 * @internal
 */
final class FilesTableTest extends DatabaseTestCase
{
    /**
     * @var PDO
     */
    private $pdo;

    protected function setUp(): void
    {
        $this->pdo = $this->getPDO();
        $this->migrateDatabase($this->pdo);
    }

    public function testUniqueIndexForDirAndFileAndLanguage(): void
    {
        $this->pdo->exec("INSERT INTO files (directory, file_name, file_language) VALUES ('foo', 'a.xml', 'en')");
        $this->pdo->exec("INSERT INTO files (directory, file_name, file_language) VALUES ('foo', 'a.xml', 'fr')");
        $this->pdo->exec("INSERT INTO files (directory, file_name, file_language) VALUES ('foo', 'b.xml', 'en')");
        $this->pdo->exec("INSERT INTO files (directory, file_name, file_language) VALUES ('bar', 'a.xml', 'en')");
        self::expectException(\PDOException::class);
        self::expectExceptionMessage('UNIQUE constraint failed: files.directory, files.file_name, files.file_language');
        $this->pdo->exec("INSERT INTO files (directory, file_name, file_language) VALUES ('foo', 'a.xml', 'en')");
    }
}
