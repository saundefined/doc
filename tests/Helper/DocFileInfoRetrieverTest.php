<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\Helper;

use PhpDotNet\DocTools\Helper\DocFileInfoRetriever;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class DocFileInfoRetrieverTest extends TestCase
{
    public function testGetRevisionWithCorrectValues(): void
    {
        $extract = '<!-- $Revision: 347269 $ -->';
        static::assertSame('347269', DocFileInfoRetriever::getRevision($extract));
    }

    public function testGetRevisionWithInvalidValues(): void
    {
        $extract = '<!-- $Revision: zxgrg $ -->';
        static::assertSame('0', DocFileInfoRetriever::getRevision($extract));

        $extract = 'This has no tag';
        static::assertSame('0', DocFileInfoRetriever::getRevision($extract));
    }

    /* None of them should have an SVN revision anymore */
    public function testGetTranslatedFileInformationStandardCaseSVNRevision(): void
    {
        $extract = '<?xml version="1.0" encoding="utf-8"?>
<!-- $Revision: 330344 $ -->
<!-- EN-Revision: 330340 Maintainer: yannick Status: ready -->
<!-- Reviewed: yes -->
<!-- State: experimental -->';
        $translationInfo = DocFileInfoRetriever::getTranslatedFileInformation($extract);
        static::assertSame('NULL', $translationInfo->getEnglishRevision());
        static::assertSame('NULL', $translationInfo->getMaintainer());
        static::assertSame('NULL', $translationInfo->getStatus());
    }

    public function testGetTranslatedFileInformationStandardCaseGitRevision(): void
    {
        $extract = '<?xml version="1.0" encoding="utf-8"?>
<!-- $Revision: 330344 $ -->
<!-- EN-Revision: 17c2ee920116805de89543e839f0eefe0cd051b3 Maintainer: yannick Status: ready -->
<!-- Reviewed: yes -->
<!-- State: experimental -->';
        $translationInfo = DocFileInfoRetriever::getTranslatedFileInformation($extract);
        static::assertSame('17c2ee920116805de89543e839f0eefe0cd051b3', $translationInfo->getEnglishRevision());
        static::assertSame('yannick', $translationInfo->getMaintainer());
        static::assertSame('ready', $translationInfo->getStatus());
    }

    public function testGetTranslatedFileInformationNotApplicableCase(): void
    {
        $extract = '<?xml version="1.0" encoding="utf-8"?>
<!-- $Revision: 330344 $ -->
<!-- EN-Revision: n/a Maintainer: yannick Status: ready -->
<!-- Reviewed: yes -->
<!-- State: experimental -->';
        $translationInfo = DocFileInfoRetriever::getTranslatedFileInformation($extract);
        static::assertSame('n/a', $translationInfo->getEnglishRevision());
        static::assertSame('yannick', $translationInfo->getMaintainer());
        static::assertSame('ready', $translationInfo->getStatus());
    }

    public function testGetTranslatedFileInformationBogusCase(): void
    {
        $extract = 'this is random info which does not match the regex';
        $translationInfo = DocFileInfoRetriever::getTranslatedFileInformation($extract);
        static::assertSame('NULL', $translationInfo->getEnglishRevision());
        static::assertSame('NULL', $translationInfo->getMaintainer());
        static::assertSame('NULL', $translationInfo->getStatus());
    }
}
