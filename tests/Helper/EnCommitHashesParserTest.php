<?php

namespace Tests\PhpDotNet\DocTools\Helper;

use PhpDotNet\DocTools\Helper\EnCommitHashesParser;
use PHPUnit\Framework\TestCase;

final class EnCommitHashesParserTest extends TestCase
{
    public function testBasicParsing(): void
    {
        $expected = [
            'language/control-structures/include.xml' => '4774ba9dba917de68290c5d8bede51cf040d6fcd',
            'language/oop5/decon.xml' => '72880807af948704e2dd806ff38b08112d39469c',
            'reference/ldap/using.xml' => '72880807af948704e2dd806ff38b08112d39469c',
            'reference/memcache/memcache/close.xml' => '72880807af948704e2dd806ff38b08112d39469c',
            'reference/mysqli/mysqli/info.xml' => '72880807af948704e2dd806ff38b08112d39469c',
            'reference/ps/functions/ps-end-page.xml' => '72880807af948704e2dd806ff38b08112d39469c',
            'reference/weakref/weakmap.xml' => '72880807af948704e2dd806ff38b08112d39469c',
        ];
        $input = "4774ba9dba917de68290c5d8bede51cf040d6fcd\n"
            . ":100644 100644 6c6318a502 3d4534b340 M	language/control-structures/include.xml\n"
            . "\n"
            . "72880807af948704e2dd806ff38b08112d39469c\n"
            . "\n"
            . ":100644 100644 a8222a9f16 c56d71c6dd M	language/oop5/decon.xml\n"
            . ":100644 100644 77636182fc ce7f6ebae4 M	reference/ldap/using.xml\n"
            . ":100644 100644 8688d06d7a 8b2c5bdc66 M	reference/memcache/memcache/close.xml\n"
            . ":100644 100644 7e5b1a6941 ec6ff8e4ec M	reference/mysqli/mysqli/info.xml\n"
            . ":100644 100644 026734e47a 496fc9dc6e M	reference/ps/functions/ps-end-page.xml\n"
            . ":100644 100644 56d1bbe86d c0728a80a9 M	reference/weakref/weakmap.xml\n";
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame($expected, $output);
    }

    public function testOnlyMostRecentCommitIsSaved(): void
    {
        $expected = [
            'language/control-structures/include.xml' => '4774ba9dba917de68290c5d8bede51cf040d6fcd',
            'reference/ldap/using.xml' => '4774ba9dba917de68290c5d8bede51cf040d6fcd',
            'language/oop5/decon.xml' => '72880807af948704e2dd806ff38b08112d39469c',
        ];
        $input = "4774ba9dba917de68290c5d8bede51cf040d6fcd\n"
            . ":100644 100644 6c6318a502 3d4534b340 M	language/control-structures/include.xml\n"
            . ":100644 100644 77636182fc ce7f6ebae4 M	reference/ldap/using.xml\n"
            . "\n"
            . "72880807af948704e2dd806ff38b08112d39469c\n"
            . "\n"
            . ":100644 100644 a8222a9f16 c56d71c6dd M	language/oop5/decon.xml\n"
            . ":100644 100644 77636182fc ce7f6ebae4 M	reference/ldap/using.xml\n";
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame($expected, $output);
    }

    public function testDoesNoChokeOnWeirdOutputEnding()
    {
        $input = "268699f9f31f548f38ca074c748a151503ff1a2f\n"
            . "\n"
            . ":100644 100644 824f3671ed 21e9ce4523 M	chapters/install.xml\n"
            . ":100644 100644 faaaaae5db d34cced2eb M	functions/pgsql.xml\n"
            . "ec1ea24811105ebafb02b842c98195186f24cfa1\n"
            . "\n"
            . ":000000 100644 0000000000 b457ae06e1 A	appendices/debugger.xml\n"
            . ":000000 100644 0000000000 567fbc971e A	functions/xml.xml\n"
            . ":000000 100644 0000000000 2a5f21310e A	functions/zlib.xml\n"
            . ":000000 100644 0000000000 448b69ff84 A	preface.xml\n"
            . "02de0f51ef48916b432c652b309803a47074a9c2\n"
            . "35228027b5cebe9b6ba6284604b8bf4969ccbc4b\n";
        $expected = [
            'chapters/install.xml' => '268699f9f31f548f38ca074c748a151503ff1a2f',
            'functions/pgsql.xml' => '268699f9f31f548f38ca074c748a151503ff1a2f',
            'appendices/debugger.xml' => 'ec1ea24811105ebafb02b842c98195186f24cfa1',
            'functions/xml.xml' => 'ec1ea24811105ebafb02b842c98195186f24cfa1',
            'functions/zlib.xml' => 'ec1ea24811105ebafb02b842c98195186f24cfa1',
            'preface.xml' => 'ec1ea24811105ebafb02b842c98195186f24cfa1',
        ];
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame($expected, $output);
    }

    public function testIgnoredFiles(): void
    {
        $input = "268699f9f31f548f38ca074c748a151503ff1a2f\n"
            . "\n"
            // Invalid extension
            . ":100644 100644 824f3671ed 21e9ce4523 M	chapters/install.txt\n"
            // Invalid top dirs
            . ":100644 100644 faaaaae5db d34cced2eb M	chmonly/pgsql.xml\n"
            . ":000000 100644 0000000000 b457ae06e1 A	internals/debugger.xml\n"
            . ":000000 100644 0000000000 567fbc971e A	internals2/xml.xml\n"
            // Invalid files
            . ":000000 100644 0000000000 2a5f21310e A	functions/contributors.ent\n"
            . ":000000 100644 0000000000 2a5f21310e A	functions/contributors.xml\n"
            . ":000000 100644 0000000000 2a5f21310e A	functions/missing-ids.xml\n"
            . ":000000 100644 0000000000 2a5f21310e A	functions/license.xml\n"
            . ":000000 100644 0000000000 2a5f21310e A	functions/translation.xml\n"
            . ":000000 100644 0000000000 2a5f21310e A	functions/versions.xml\n"
            // Invalid paths
            . ":000000 100644 0000000000 2a5f21310e A	appendices/extensions.xml\n"
            . ":000000 100644 0000000000 448b69ff84 A	appendices/reserved.constants.xml\n";
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame([], $output);
    }

    public function testRenamedFiles(): void
    {
        $expected = [
            'new-file-name.xml' => '268699f9f31f548f38ca074c748a151503ff1a2f',
        ];
        $input = "268699f9f31f548f38ca074c748a151503ff1a2f\n"
            . "\n"
            . ":100644 000000 937028bb3e 0000000000 M	new-file-name.xml\n"
            . "ec1ea24811105ebafb02b842c98195186f24cfa1\n"
            . "\n"
            . ":100644 100644 78e1a6bb3d 1e18bb8c14 R085	old-file-name.xml	new-file-name.xml\n"
            . "2f493dfdc26fe0736d693ceba7986af511729dbb\n"
            . "\n"
            . ":100644 100644 dd6f70e0c3 954f087b67 M	old-file-name.xml\n";
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame($expected, $output);
    }

    public function testDeletedFiles(): void
    {
        $expected = [
            'reference/oci8/functions/ocifetchinto.xml' => '268699f9f31f548f38ca074c748a151503ff1a2f',
        ];
        $input = "268699f9f31f548f38ca074c748a151503ff1a2f\n"
            . "\n"
            . ":100644 000000 937028bb3e 0000000000 D	reference/oci8/functions/ocicommit.xml\n"
            . ":100644 100644 dd6f70e0c3 954f087b67 M	reference/oci8/functions/ocifetchinto.xml\n"
            . "ec1ea24811105ebafb02b842c98195186f24cfa1\n"
            . "\n"
            . ":100644 100644 dd6f70e0c3 954f087b67 M	reference/oci8/functions/ocicommit.xml\n"
            . ":100644 100644 dd6f70e0c3 954f087b67 M	reference/oci8/functions/ocifetchinto.xml\n";
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame($expected, $output);
    }

    public function testAddedFileAfterBeingDeleted(): void
    {
        $expected = [
            'reference/oci8/functions/ocicommit.xml' => '268699f9f31f548f38ca074c748a151503ff1a2f',
        ];
        $input = "268699f9f31f548f38ca074c748a151503ff1a2f\n"
            . "\n"
            . ":100644 000000 937028bb3e 0000000000 M	reference/oci8/functions/ocicommit.xml\n"
            . "ec1ea24811105ebafb02b842c98195186f24cfa1\n"
            . "\n"
            . ":100644 100644 dd6f70e0c3 954f087b67 A	reference/oci8/functions/ocicommit.xml\n"
            . "2f493dfdc26fe0736d693ceba7986af511729dbb\n"
            . "\n"
            . ":100644 100644 dd6f70e0c3 954f087b67 D	reference/oci8/functions/ocicommit.xml\n"
            . "1bd0fbbce1c6daaaf441ae4d9b0d1e1e3778789e\n"
            . "\n"
            . ":100644 100644 dd6f70e0c3 954f087b67 M	reference/oci8/functions/ocicommit.xml\n";
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame($expected, $output);
    }

    public function testIgnoreGitCommit(): void
    {
        $expected = [
            'reference/ev/book.xml' => 'ec1ea24811105ebafb02b842c98195186f24cfa1',
        ];
        $input = "8d666e819852f6b0561b40fcf8689b747568865c\n"
            . "\n"
            . ":100644 100644 f59f266294 49bff4284e M	reference/ev/book.xml\n"
            . "ec1ea24811105ebafb02b842c98195186f24cfa1\n"
            . "\n"
            . ":100644 100644 f59f266294 49bff4284e M	reference/ev/book.xml\n";
        $output = EnCommitHashesParser::parseGitLogOutput($input);
        static::assertSame($expected, $output);
    }
}
