<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\Renderer;

use Parsedown;
use PhpDotNet\DocTools\Renderer\HtmlRenderer;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class HtmlRendererTest extends TestCase
{
    /**
     * @var HtmlRenderer
     */
    private $renderer;

    protected function setUp(): void
    {
        $this->renderer = new HtmlRenderer(new Parsedown());
    }

    public function testRenderNoLayout(): void
    {
        $markdown = '# Hello World';
        static::assertSame('<h1>Hello World</h1>', $this->renderer->render($markdown));
    }

    public function testRenderBasicLayout(): void
    {
        $layout = '<section>' . HtmlRenderer::LAYOUT_CONTENT_TAG . '</section>';
        $markdown = '# Hello World';
        static::assertSame('<section><h1>Hello World</h1></section>', $this->renderer->render($markdown, $layout));
    }
}
