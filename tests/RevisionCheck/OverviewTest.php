<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\RevisionCheck;

use PhpDotNet\DocTools\RevisionCheck\Overview;
use PhpDotNet\DocTools\RevisionCheck\Status\MissingRevision;
use PhpDotNet\DocTools\RevisionCheck\Status\NoUpstream;
use PhpDotNet\DocTools\RevisionCheck\Status\Outdated;
use PhpDotNet\DocTools\RevisionCheck\Status\Translation;
use PhpDotNet\DocTools\RevisionCheck\Status\Untranslated;
use PhpDotNet\DocTools\RevisionCheck\Status\UpToDate;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class OverviewTest extends TestCase
{
    /**
     * @var Overview
     */
    private $overview;

    protected function setUp(): void
    {
        $this->overview = new Overview();
        $upToDate = new UpToDate(75, 0);
        $outDated = new Outdated(0, 0);
        $missing = new MissingRevision(0, 0);
        $untranslated = new Untranslated(25, 0);
        $noUpstream = new NoUpstream(0, 0);
        $this->overview->addTranslation(
            'de',
            new Translation($upToDate, $outDated, $missing, $untranslated, $noUpstream)
        );
        $upToDate = new UpToDate(30, 0);
        $untranslated = new Untranslated(60, 0);
        $this->overview->addTranslation(
            'fr',
            new Translation($upToDate, $outDated, $missing, $untranslated, $noUpstream)
        );
    }

    public function testGetUpToDatePercentageAsCVS(): void
    {
        static::assertSame('75, 33.33', $this->overview->getUpToDatePercentageAsCVS());
    }

    public function testGetLanguagesAsCVS(): void
    {
        static::assertSame("'de', 'fr'", $this->overview->getLanguagesAsCVS());
    }
}
