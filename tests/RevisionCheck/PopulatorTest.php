<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\RevisionCheck;

use DateTimeImmutable;
use PDO;
use PhpDotNet\DocTools\RevisionCheck\Populator;
use Tests\PhpDotNet\DocTools\TestCases\DatabaseTestCase;

/**
 * @internal
 */
final class PopulatorTest extends DatabaseTestCase
{
    /**
     * @var PDO
     */
    private $pdo;
    /**
     * @var Populator
     */
    private $populator;

    protected function setUp(): void
    {
        $this->pdo = $this->getPDO();
        $this->migrateDatabase($this->pdo);
        $this->populator = new Populator($this->pdo);
    }

    public function testStandardFileCase(): void
    {
        $time = DateTimeImmutable::createFromFormat('U', '1570233057');

        $this->populator->insertFileRevisionCheck(
            'foo',
            'bar',
            'en',
            '1552',
            'maintainer',
            5,
            $time
        );

        $row = $this->pdo->query('SELECT * FROM files')->fetch();

        static::assertSame('foo', $row->directory);
        static::assertSame('bar', $row->file_name);
        static::assertSame('en', $row->file_language);
        static::assertSame('1552', $row->revision);
        static::assertSame('maintainer', $row->maintainer);
        static::assertSame('5', $row->size);
        // SQLite stores and returns everything as strings
        static::assertSame('2019-10-04 23:50:57', $row->updated_at);
    }

    public function testNullMaintainer(): void
    {
        $this->populator->insertFileRevisionCheck(
            'foo',
            'bar',
            'en',
            '1552',
            'NULL',
            5,
            new DateTimeImmutable()
        );

        $row = $this->pdo->query('SELECT maintainer FROM files')->fetch();

        static::assertNull($row->maintainer);
    }

    public function testNullRevision(): void
    {
        $this->populator->insertFileRevisionCheck(
            'foo',
            'bar',
            'en',
            'NULL',
            'maintainer',
            5,
            new DateTimeImmutable()
        );

        $row = $this->pdo->query('SELECT revision FROM files')->fetch();

        static::assertNull($row->revision);
    }

    public function testPopulatorCounter(): void
    {
        $this->populator->insertFileRevisionCheck(
            'foo',
            'bar',
            'fr',
            '1552',
            'maintainer',
            5,
            new DateTimeImmutable()
        );
        $this->populator->insertFileRevisionCheck(
            'foo',
            'bar',
            'es',
            '1552',
            'maintainer',
            5,
            new DateTimeImmutable()
        );
        $this->populator->insertFileRevisionCheck(
            'foo',
            'bar',
            'en',
            '1552',
            'maintainer',
            5,
            new DateTimeImmutable()
        );
        $this->populator->insertFileRevisionCheck(
            'foo',
            'bar',
            'de',
            '1552',
            'maintainer',
            5,
            new DateTimeImmutable()
        );

        static::assertSame(4, $this->populator->getNumberOfFilesPopulated());
    }
}
