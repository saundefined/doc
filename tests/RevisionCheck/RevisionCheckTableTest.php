<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\RevisionCheck;

use PhpDotNet\DocTools\RevisionCheck\RevisionCheckTable;
use Tests\PhpDotNet\DocTools\TestCases\DatabaseTestCase;

/**
 * @internal
 */
final class RevisionCheckTableTest extends DatabaseTestCase
{
    private const LANGUAGE = 'fr';
    /**
     * @var RevisionCheckTable
     */
    private $revCheckTable;

    protected function setUp(): void
    {
        $pdo = $this->getPDO();
        $this->migrateDatabase($pdo);
        $this->seedDatabase($pdo);
        $this->revCheckTable = new RevisionCheckTable($pdo);
    }

    public function testGetTranslationStatus(): void
    {
        $translation = $this->revCheckTable->getTranslationStatus('fr');
        static::assertIsObject($translation);
        static::assertSame(500, $translation->getUpToDateAmount());
        static::assertSame(200, $translation->getOutdatedAmount());
        static::assertSame(15, $translation->getMissingRevisionAmount());
        static::assertSame(20, $translation->getNoUpstreamAmount());
        static::assertSame(400, $translation->getUntranslatedAmount());
    }

    public function testGetUntranslated(): void
    {
        $result = $this->revCheckTable->getUntranslated(self::LANGUAGE);
        static::assertCount(400, $result);
    }

    public function testGetOutdated(): void
    {
        $result = $this->revCheckTable->getOutdated(self::LANGUAGE);
        static::assertCount(200, $result);
    }

    public function testGetMissingRevision(): void
    {
        $result = $this->revCheckTable->getMissingRevision(self::LANGUAGE);
        static::assertCount(15, $result);
    }

    public function testGetNoUpstream(): void
    {
        $result = $this->revCheckTable->getNoUpstream(self::LANGUAGE);
        static::assertCount(20, $result);
    }
}
