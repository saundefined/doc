<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\RevisionCheck\Status;

use PhpDotNet\DocTools\RevisionCheck\Status\Files;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class FilesTest extends TestCase
{
    public function testNormalCase(): void
    {
        $files = self::getFiles(10, 5);

        static::assertSame(10, $files->amount());
        static::assertSame(5, $files->size());
    }

    public function testThrowOnInvalidAmount(): void
    {
        self::expectException(\InvalidArgumentException::class);
        self::expectExceptionMessage('Amount of files must be greater or equal than zero');
        $this->getFiles(-1, 0);
    }

    public function testThrowOnInvalidSize(): void
    {
        self::expectException(\InvalidArgumentException::class);
        self::expectExceptionMessage('Size of files must be greater or equal than zero');
        $this->getFiles(0, -1);
    }

    private function getFiles(int $amount, int $size): Files
    {
        return self::getMockForAbstractClass(Files::class, [$amount, $size]);
    }
}
