<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\RevisionCheck\Status;

use PhpDotNet\DocTools\RevisionCheck\Status\MissingRevision;
use PhpDotNet\DocTools\RevisionCheck\Status\Outdated;
use PhpDotNet\DocTools\RevisionCheck\Status\Total;
use PhpDotNet\DocTools\RevisionCheck\Status\Untranslated;
use PhpDotNet\DocTools\RevisionCheck\Status\UpToDate;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class TotalTest extends TestCase
{
    public function testValidCase(): void
    {
        $upToDate = new UpToDate(25, 250);
        $outDated = new Outdated(20, 200);
        $missing = new MissingRevision(5, 50);
        $untranslated = new Untranslated(50, 500);
        $total = new Total($upToDate, $outDated, $missing, $untranslated);

        static::assertSame(100, $total->amount());
        static::assertSame(1000, $total->size());
    }
}
