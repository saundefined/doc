<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\RevisionCheck\Status;

use PhpDotNet\DocTools\RevisionCheck\Status\MissingRevision;
use PhpDotNet\DocTools\RevisionCheck\Status\NoUpstream;
use PhpDotNet\DocTools\RevisionCheck\Status\Outdated;
use PhpDotNet\DocTools\RevisionCheck\Status\Translation;
use PhpDotNet\DocTools\RevisionCheck\Status\Untranslated;
use PhpDotNet\DocTools\RevisionCheck\Status\UpToDate;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class TranslationTest extends TestCase
{
    /**
     * @var Translation
     */
    private $translation;

    protected function setUp(): void
    {
        $upToDate = new UpToDate(25, 250);
        $outDated = new Outdated(20, 200);
        $missing = new MissingRevision(5, 50);
        $untranslated = new Untranslated(50, 500);
        $noUpstream = new NoUpstream(10, 40);
        $this->translation = new Translation($upToDate, $outDated, $missing, $untranslated, $noUpstream);
    }

    public function testGetMissingRevisionTagVariation(): void
    {
        static::assertSame(5, $this->translation->getMissingRevisionAmount());
        static::assertSame(5., $this->translation->getMissingRevisionAmountPercentage());
        static::assertSame(50, $this->translation->getMissingRevisionSize());
        static::assertSame(5., $this->translation->getMissingRevisionSizePercentage());
    }

    public function testGetOutDatedVariation(): void
    {
        static::assertSame(20, $this->translation->getOutdatedAmount());
        static::assertSame(20., $this->translation->getOutdatedAmountPercentage());
        static::assertSame(200, $this->translation->getOutdatedSize());
        static::assertSame(20., $this->translation->getOutdatedSizePercentage());
    }

    public function testGetUntranslatedVariation(): void
    {
        static::assertSame(50, $this->translation->getUntranslatedAmount());
        static::assertSame(50., $this->translation->getUntranslatedAmountPercentage());
        static::assertSame(500, $this->translation->getUntranslatedSize());
        static::assertSame(50., $this->translation->getUntranslatedSizePercentage());
    }

    public function testGetTotalVariation(): void
    {
        static::assertSame(100, $this->translation->getTotalAmount());
        static::assertSame(100., $this->translation->getTotalAmountPercentage());
        static::assertSame(1000, $this->translation->getTotalSize());
        static::assertSame(100., $this->translation->getTotalSizePercentage());
    }

    public function testGetUpToDateVariation(): void
    {
        static::assertSame(25, $this->translation->getUpToDateAmount());
        static::assertSame(25., $this->translation->getUpToDateAmountPercentage());
        static::assertSame(250, $this->translation->getUpToDateSize());
        static::assertSame(25., $this->translation->getUpToDateSizePercentage());
    }

    public function testGetNoUpstreamVariation(): void
    {
        static::assertSame(10, $this->translation->getNoUpstreamAmount());
        static::assertSame(40, $this->translation->getNoUpstreamSize());
    }

    public function testEmptyTranslation(): void
    {
        $upToDate = new UpToDate(0, 0);
        $outDated = new Outdated(0, 0);
        $missing = new MissingRevision(0, 0);
        $untranslated = new Untranslated(0, 0);
        $noUpstream = new NoUpstream(0, 0);
        $translation = new Translation($upToDate, $outDated, $missing, $untranslated, $noUpstream);

        static::assertSame(0, $translation->getTotalAmount());
        static::assertSame(0., $translation->getTotalAmountPercentage());
        static::assertSame(0, $translation->getTotalSize());
        static::assertSame(0., $translation->getTotalSizePercentage());
    }
}
