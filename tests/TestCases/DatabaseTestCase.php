<?php

declare(strict_types=1);

namespace Tests\PhpDotNet\DocTools\TestCases;

use PDO;
use Phinx\Config\Config;
use Phinx\Migration\Manager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

/**
 * @internal
 */
class DatabaseTestCase extends TestCase
{
    protected function getPDO(): PDO
    {
        return new PDO('sqlite::memory:', null, null, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ]);
    }

    protected function migrateDatabase(PDO $pdo): void
    {
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_BOTH);
        $this->getManager($pdo)->migrate('unit');
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    protected function seedDatabase(PDO $pdo): void
    {
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_BOTH);
        $this->getManager($pdo)->migrate('unit');
        $this->getManager($pdo)->seed('unit');
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    private function getManager(PDO $pdo): Manager
    {
        $configArray = require 'phinx.php';
        $configArray['environments']['unit'] = [
            'adapter' => 'sqlite',
            'connection' => $pdo,
        ];
        $config = new Config($configArray);

        return new Manager($config, new StringInput(' '), new NullOutput());
    }
}
